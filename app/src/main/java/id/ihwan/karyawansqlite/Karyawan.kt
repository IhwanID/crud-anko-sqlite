package id.ihwan.karyawansqlite


/**
 * Created by Ihwan ID on 22,October,2018.
 * Subscribe my Youtube Channel => https://www.youtube.com/channel/UCjntzibNSsjjIOh0HoP9vxw
 * mynameisihwan@gmail.com
 */
data class Karyawan(var id: Long?,
                    var nama:String?,
                    val jabatan: String?,
                    val asal: String?) {

    companion object {
        const val TABLE_KARYAWAN: String = "TABLE_KARYAWAN"
        const val ID: String = "ID_"
        const val NAMA: String = "NAMA"
        const val JABATAN: String = "JABATAN"
        const val ASAL: String = "ASAL"
    }

}