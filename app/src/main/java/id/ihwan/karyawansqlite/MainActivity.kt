package id.ihwan.karyawansqlite

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.db.insert
import org.jetbrains.anko.db.update
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        var oldNama = intent.getStringExtra("oldNama")
        val oldJabatan = intent.getStringExtra("oldJabatan")
        val oldAsal = intent.getStringExtra("oldAsal")

        if (oldNama.isNullOrBlank()) {
            buttonUpdate.isEnabled = false
        } else {
            buttonSave.isEnabled = false

            edtNama.setText(oldNama)
            edtJabatan.setText(oldJabatan)
            edtAsal.setText(oldAsal)
        }

            buttonSave.setOnClickListener {
                addkaryawan()

                edtNama.text.clear()
                edtJabatan.text.clear()
                edtAsal.text.clear()
            }

            buttonView.setOnClickListener {
                startActivity<ListKaryawanActivity>()
            }

            buttonUpdate.setOnClickListener {
                database.use {
                    update(
                        Karyawan.TABLE_KARYAWAN,
                        Karyawan.NAMA to edtNama.text.toString(),
                        Karyawan.JABATAN to edtJabatan.text.toString(),
                        Karyawan.ASAL to edtAsal.text.toString()
                    ).whereArgs(
                        "${Karyawan.NAMA} = {nama}",
                        "nama" to oldNama
                    ).exec()
                }

                toast("Data Diedit")
            }


        }

    private fun addkaryawan() {
        database.use {
            insert(Karyawan.TABLE_KARYAWAN,
                Karyawan.NAMA to edtNama.text.toString(),
                Karyawan.JABATAN to edtJabatan.text.toString(),
                Karyawan.ASAL to edtAsal.text.toString()
            )
            toast("Data Ditambahkan")
        }

    }

}
