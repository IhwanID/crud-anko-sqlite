package id.ihwan.karyawansqlite

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import kotlinx.android.synthetic.main.item_karyawan.view.*
import org.jetbrains.anko.db.delete
import org.jetbrains.anko.startActivity
import org.jetbrains.anko.toast


/**
 * Created by Ihwan ID on 22,October,2018.
 * Subscribe my Youtube Channel => https://www.youtube.com/channel/UCjntzibNSsjjIOh0HoP9vxw
 * mynameisihwan@gmail.com
 */
class KaryawanAdapter(val context: Context, val items: ArrayList<Karyawan>)
    : RecyclerView.Adapter<KaryawanAdapter.ViewHolder>(){

    override fun onCreateViewHolder(p0: ViewGroup, p1: Int): ViewHolder {
        return ViewHolder(LayoutInflater.from(context)
            .inflate(R.layout.item_karyawan, p0, false))
    }

    override fun getItemCount(): Int {
        return items.size
    }

    override fun onBindViewHolder(p0: ViewHolder, p1: Int) {
       p0.bindItem(items[p1])
    }

    class ViewHolder(view: View) : RecyclerView.ViewHolder(view){

        fun bindItem(items : Karyawan){

            itemView.namaKaryawan.text = items.nama
            itemView.jabatanKaryawan.text = items.jabatan
            itemView.asalKaryawan.text = items.asal

            itemView.btnEdit.setOnClickListener {
                itemView.context.startActivity<MainActivity>(
                    "oldNama" to items.nama,
                    "oldJabatan" to items.jabatan,
                    "oldAsal" to items.asal
                )
               // itemView.context.toast("Data Diedit")
            }

            itemView.btnDelete.setOnClickListener {
                itemView.context.database.use {
                    delete(Karyawan.TABLE_KARYAWAN, "(${Karyawan.NAMA} = {nama})",
                        "nama" to items.nama.toString())
                }

                Toast.makeText(itemView.context, "Data DiHapus", Toast.LENGTH_SHORT).show()
            }
        }
    }
}