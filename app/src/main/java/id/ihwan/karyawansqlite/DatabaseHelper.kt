package id.ihwan.karyawansqlite

import android.content.Context
import android.database.sqlite.SQLiteDatabase
import org.jetbrains.anko.db.*


/**
 * Created by Ihwan ID on 22,October,2018.
 * Subscribe my Youtube Channel => https://www.youtube.com/channel/UCjntzibNSsjjIOh0HoP9vxw
 * mynameisihwan@gmail.com
 */
class DatabaseHelper(ctx: Context)
    : ManagedSQLiteOpenHelper(ctx, "Karyawan.db", null, 1)  {

    companion object {

        private var instance: DatabaseHelper? = null

        @Synchronized
    fun getInstance(ctx: Context): DatabaseHelper{

            if (instance == null){
                instance = DatabaseHelper(ctx.applicationContext)
            }

            return instance as DatabaseHelper
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(Karyawan.TABLE_KARYAWAN, true,
            Karyawan.ID to INTEGER + PRIMARY_KEY + AUTOINCREMENT,
            Karyawan.NAMA to TEXT,
            Karyawan.JABATAN to TEXT,
            Karyawan.ASAL to TEXT)
    }

    override fun onUpgrade(db: SQLiteDatabase?, oldVersion: Int, newVersion: Int) {
        db?.dropTable(Karyawan.TABLE_KARYAWAN, true)
    }
}

val Context.database : DatabaseHelper
    get() = DatabaseHelper.getInstance(applicationContext)