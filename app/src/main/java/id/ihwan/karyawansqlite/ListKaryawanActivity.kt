package id.ihwan.karyawansqlite

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import kotlinx.android.synthetic.main.activity_list_karyawan.*
import org.jetbrains.anko.db.classParser
import org.jetbrains.anko.db.select

class ListKaryawanActivity : AppCompatActivity() {

    private var karyawan = ArrayList<Karyawan>()
    private lateinit var adapter: KaryawanAdapter

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_list_karyawan)

        adapter = KaryawanAdapter(this, karyawan)
        recyclerView.adapter = adapter
        getData()

        recyclerView.layoutManager = LinearLayoutManager(this)

    }

    private fun getData() {
        database.use {
            karyawan.clear()
            val result = select(Karyawan.TABLE_KARYAWAN)
            val dataKaryawan = result.parseList(classParser<Karyawan>())
            karyawan.addAll(dataKaryawan)
            adapter.notifyDataSetChanged()
        }
    }


}
